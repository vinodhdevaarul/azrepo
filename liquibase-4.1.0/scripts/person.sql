-- person.sql
--preconditions onFail:CONTINUE
--preconditions not tableExists tableName:person schemaName:orcl
CREATE TABLE person (
  id          NUMBER,
  name        VARCHAR2(100),
  description VARCHAR2(50),
  CONSTRAINT tab1_pk PRIMARY KEY (id)
);

INSERT INTO person (id, name, description) VALUES (person_seq.NEXTVAL, 'Ajay', 'Description for ' || person_seq.CURRVAL);
COMMIT;