-- course.sql
--preconditions onFail:CONTINUE
--preconditions not tableExists tableName:course schemaName:orcl
CREATE TABLE course (
  course_id          NUMBER,
  course_name        VARCHAR2(100),
  description VARCHAR2(50)
  );