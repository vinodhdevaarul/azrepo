-- address.sql
--preconditions onFail:CONTINUE
--preconditions not tableExists tableName:address schemaName:orcl
CREATE TABLE address (
  id          NUMBER,
  address1   VARCHAR2(100),
  address2   VARCHAR2(50),
  pincode    number(10),
  city       varchar2(40),
  country    varchar2(40),
  CONSTRAINT address_pk PRIMARY KEY (id)
);

INSERT INTO address (id, address1, address2, pincode, city, country) VALUES (person_seq.NEXTVAL, 'Main Road', 'Cut Road', 9876543, 'Chennai', 'India');
COMMIT;