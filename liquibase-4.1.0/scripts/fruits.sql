-- fruits.sql
--preconditions onFail:CONTINUE
--preconditions not tableExists tableName:fruits schemaName:orcl
CREATE TABLE fruits (
  id          NUMBER,
  name        VARCHAR2(100),
  description VARCHAR2(50)
);
